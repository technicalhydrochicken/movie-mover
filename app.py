#!/usr/bin/env python3
import sys
import argparse
import os
import subprocess
import shutil
import glob
import logging
from datetime import datetime, timedelta
from plexapi.myplex import MyPlexAccount


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(
        description="Script for moving videos once they have been compressed")
    parser.add_argument("-v",
                        "--verbose",
                        help="Be verbose",
                        action="store_true",
                        dest="verbose")
    parser.add_argument('-d',
                        '--dry-run',
                        action='store_true',
                        help='Just print what we are going to move')
    parser.add_argument('-p',
                        '--plex-instance',
                        help='Plex instance to update')
    parser.add_argument('-s',
                        '--plex-section',
                        help="Section of plex to update")
    parser.add_argument('source', help='Source directory for movies')
    parser.add_argument('destination', help='Destination directory for movies')

    return parser.parse_args()


def main():
    args = parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.WARN)

    move_movies = []
    now = datetime.now()
    minimum_age = timedelta(minutes=5)

    logging.info(f"About to iterate through {args.source}")
    for item in os.listdir(args.source):
        logging.debug(f"Evaluating {item} for moving")
        movie_dir = os.path.join(args.source, item)

        compressed_markers = glob.glob(f"{movie_dir}/.compressed.*.marked")
        if len(compressed_markers) == 0:
            logging.debug(
                f"Skipping {movie_dir} since it does not contain a compression marker"
            )
            continue

        compressed_at = datetime.fromtimestamp(
            os.path.getmtime(compressed_markers[0]))

        age = now - compressed_at
        if age < minimum_age:
            logging.debug(f"{item} is only age {age}, skipping")
            continue

        logging.info(f"Adding {movie_dir} to move list")
        move_movies.append(movie_dir)

    logging.debug(f"The following films will be moved {move_movies}")
    for source in move_movies:
        logging.debug(f"Moving {source}")
        rsync_cmd = ['rsync', '-va', '--progress', source, args.destination]
        if args.dry_run:
            rsync_cmd.append('--dry-run')
        logging.info(f"Running {rsync_cmd}")
        subprocess.check_call(rsync_cmd)

        if not args.dry_run:
            logging.info(f"Removing {source}")
            shutil.rmtree(source)
    if len(move_movies) >= 0:
        if args.plex_instance:
            print("Updating plex server")
            plex_username = os.environ['PLEX_USERNAME']
            plex_password = os.environ['PLEX_PASSWORD']
            account = MyPlexAccount(plex_username, plex_password)
            plex = account.resource(args.plex_instance).connect()
            s = plex.library.section(args.plex_section)
            s.refresh()

    return 0


if __name__ == "__main__":
    sys.exit(main())

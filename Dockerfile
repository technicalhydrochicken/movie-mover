FROM ubuntu:20.04

COPY requirements.txt /

RUN apt-get -y update && \
    apt-get -y install --no-install-recommends \
      python3-pip python3-setuptools python3-bs4 \
      rsync && \
    pip3 install -r /requirements.txt && \
    rm -rf /var/lib/apt/lists/*

VOLUME /movie-source
VOLUME /movie-destination

COPY app.py /

ENTRYPOINT ["/app.py", "/movie-source", "/movie-destination/"]
